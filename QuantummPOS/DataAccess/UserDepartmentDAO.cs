﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Info;

namespace DataAccess
{
    public class UserDepartmentDAO
    {
        SqlConnection connection;
        SqlCommand command;

        public UserDepartmentDAO()
        {
            connection = new SqlConnection(ConfigurationManager.ConnectionStrings["QuantumnPOS"].ConnectionString);
        }

        public DataTable SelectAll()
        {
            command = new SqlCommand("UserDepartment_SelectAll", connection);
            command.CommandType = CommandType.StoredProcedure;

            DataTable dt = new DataTable();

            SqlDataAdapter adaper = new SqlDataAdapter(command);

            adaper.Fill(dt);

            return dt;
        }

        public SqlDataReader Select_All()
        {
            command = new SqlCommand("UserDepartment_SelectAll", connection);
            command.CommandType = CommandType.StoredProcedure;
            
            connection.Open();                        

            return command.ExecuteReader(CommandBehavior.CloseConnection);
        }
    }
}
