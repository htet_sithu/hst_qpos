﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Info;

namespace BusinessLogic
{
    

    public class UserDepartmentBL
    {
        UserDepartmentDAO userDAO;

        public UserDepartmentBL()
        {
            userDAO = new UserDepartmentDAO();
        }

        public DataTable SelectAll()
        {
            return userDAO.SelectAll();
        }

        public UserDepartmentCollection Select_All()
        {
            IDataReader reader = userDAO.Select_All();

            UserDepartmentCollection collection = new UserDepartmentCollection();

            while(reader.Read())
            {
                UserDepartmentInfo info = new UserDepartmentInfo();
                info.ud_ID = Convert.ToInt32(reader[0]);
                info.ud_Code = Convert.ToString(reader[1]);
                info.ud_Name = Convert.ToString(reader[2]);
                info.ud_inactive = Convert.ToBoolean(reader[3]);
                info.ud_createby = Convert.ToString(reader[4]);
                info.ud_createdate = Convert.ToDateTime(reader[5]);
                info.ud_updateby = Convert.ToString(reader[6]);
                info.ud_lastupdate = Convert.ToDateTime(reader[7]);
                collection.Add(info);
            }
            reader.Close();
            return collection;
        }
    }
}
