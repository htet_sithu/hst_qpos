﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Info
{    
    public class UserDepartmentInfo
    {
        public int ud_ID { get; set; }
        public string ud_Code { get; set; }
        public string ud_Name { get; set; }
        public bool ud_inactive { get; set; }
        public System.DateTime ud_lastupdate { get; set; }
        public string ud_updateby { get; set; }
        public System.DateTime ud_createdate { get; set; }
        public string ud_createby { get; set; }
    }

    public class UserDepartmentCollection : Collection<UserDepartmentInfo> { }
}
