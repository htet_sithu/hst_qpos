﻿namespace QuantummPOS
{
    partial class frmUserDepartment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gcUserDept = new DevExpress.XtraGrid.GridControl();
            this.gvUserDept = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gInactive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.gCreateBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gUpdateBy = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gLastUpdate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txtCode = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtName = new DevExpress.XtraEditors.TextEdit();
            this.chkInactive = new DevExpress.XtraEditors.CheckEdit();
            ((System.ComponentModel.ISupportInitialize)(this.gcUserDept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvUserDept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInactive.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // gcUserDept
            // 
            this.gcUserDept.Location = new System.Drawing.Point(12, 12);
            this.gcUserDept.MainView = this.gvUserDept;
            this.gcUserDept.Name = "gcUserDept";
            this.gcUserDept.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            this.gcUserDept.Size = new System.Drawing.Size(998, 221);
            this.gcUserDept.TabIndex = 0;
            this.gcUserDept.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvUserDept});
            this.gcUserDept.Click += new System.EventHandler(this.gcUserDept_Click);
            this.gcUserDept.DoubleClick += new System.EventHandler(this.gcUserDept_DoubleClick);
            // 
            // gvUserDept
            // 
            this.gvUserDept.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gCode,
            this.gName,
            this.gInactive,
            this.gCreateBy,
            this.gUpdateBy,
            this.gLastUpdate});
            this.gvUserDept.GridControl = this.gcUserDept;
            this.gvUserDept.Name = "gvUserDept";
            this.gvUserDept.OptionsBehavior.AutoPopulateColumns = false;
            this.gvUserDept.OptionsBehavior.Editable = false;
            // 
            // gCode
            // 
            this.gCode.Caption = "Code";
            this.gCode.FieldName = "ud_Code";
            this.gCode.Name = "gCode";
            this.gCode.Visible = true;
            this.gCode.VisibleIndex = 0;
            // 
            // gName
            // 
            this.gName.Caption = "Name";
            this.gName.FieldName = "ud_Name";
            this.gName.Name = "gName";
            this.gName.Visible = true;
            this.gName.VisibleIndex = 1;
            // 
            // gInactive
            // 
            this.gInactive.Caption = "Inactive";
            this.gInactive.ColumnEdit = this.repositoryItemCheckEdit1;
            this.gInactive.FieldName = "ud_inactive";
            this.gInactive.Name = "gInactive";
            this.gInactive.Visible = true;
            this.gInactive.VisibleIndex = 2;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // gCreateBy
            // 
            this.gCreateBy.Caption = "CreateBy";
            this.gCreateBy.FieldName = "ud_createby";
            this.gCreateBy.Name = "gCreateBy";
            this.gCreateBy.Visible = true;
            this.gCreateBy.VisibleIndex = 3;
            // 
            // gUpdateBy
            // 
            this.gUpdateBy.Caption = "UpdateBy";
            this.gUpdateBy.FieldName = "ud_updateby";
            this.gUpdateBy.Name = "gUpdateBy";
            this.gUpdateBy.Visible = true;
            this.gUpdateBy.VisibleIndex = 4;
            // 
            // gLastUpdate
            // 
            this.gLastUpdate.Caption = "LastUpdate";
            this.gLastUpdate.FieldName = "ud_lastupdate";
            this.gLastUpdate.Name = "gLastUpdate";
            this.gLastUpdate.Visible = true;
            this.gLastUpdate.VisibleIndex = 5;
            // 
            // txtCode
            // 
            this.txtCode.Location = new System.Drawing.Point(69, 258);
            this.txtCode.Name = "txtCode";
            this.txtCode.Size = new System.Drawing.Size(100, 20);
            this.txtCode.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(13, 261);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(25, 13);
            this.labelControl1.TabIndex = 2;
            this.labelControl1.Text = "Code";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(69, 369);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Save";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(13, 289);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(27, 13);
            this.labelControl2.TabIndex = 5;
            this.labelControl2.Text = "Name";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(69, 286);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(266, 20);
            this.txtName.TabIndex = 6;
            // 
            // chkInactive
            // 
            this.chkInactive.Location = new System.Drawing.Point(69, 324);
            this.chkInactive.Name = "chkInactive";
            this.chkInactive.Properties.Caption = "Inactive";
            this.chkInactive.Size = new System.Drawing.Size(75, 19);
            this.chkInactive.TabIndex = 7;
            // 
            // frmUserDepartment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1022, 573);
            this.Controls.Add(this.chkInactive);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.txtCode);
            this.Controls.Add(this.gcUserDept);
            this.Name = "frmUserDepartment";
            this.Text = "frmUserDepartment";
            this.Load += new System.EventHandler(this.frmUserDepartment_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gcUserDept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvUserDept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInactive.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gcUserDept;
        private DevExpress.XtraGrid.Views.Grid.GridView gvUserDept;
        private DevExpress.XtraGrid.Columns.GridColumn gCode;
        private DevExpress.XtraGrid.Columns.GridColumn gName;
        private DevExpress.XtraGrid.Columns.GridColumn gInactive;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gCreateBy;
        private DevExpress.XtraGrid.Columns.GridColumn gUpdateBy;
        private DevExpress.XtraGrid.Columns.GridColumn gLastUpdate;
        private DevExpress.XtraEditors.TextEdit txtCode;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtName;
        private DevExpress.XtraEditors.CheckEdit chkInactive;
    }
}