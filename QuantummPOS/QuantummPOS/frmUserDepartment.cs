﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using BusinessLogic;
using Info;
using DevExpress.Utils;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;

namespace QuantummPOS
{
    public partial class frmUserDepartment : DevExpress.XtraEditors.XtraForm
    {
        public frmUserDepartment()
        {
            InitializeComponent();
        }

        public void BindGird()
        {
            UserDepartmentBL ud = new UserDepartmentBL();

            //gcUserDept.DataSource = ud.SelectAll();
            gcUserDept.DataSource = ud.Select_All();
        }

        private void frmUserDepartment_Load(object sender, EventArgs e)
        {
            gvUserDept.OptionsBehavior.AutoPopulateColumns = false;
            
            BindGird();
        }

        private void gcUserDept_DoubleClick(object sender, EventArgs e)
        {
            //DXMouseEventArgs ea = e as DXMouseEventArgs;
            //GridView view = sender as GridView;
            //GridHitInfo info = view.CalcHitInfo(ea.Location);
            //if (info.InRow || info.InRowCell)
            //{
            //    string colCaption = info.Column == null ? "N/A" : info.Column.GetCaption();
            //    MessageBox.Show(string.Format("DoubleClick on row: {0}, column: {1}.", info.RowHandle, colCaption));
            //}

            DXMouseEventArgs ea = e as DXMouseEventArgs;
            GridView view = sender as GridView;

            //GridHitInfo info = view.CalcHitInfo(ea.Location);
            //if (info.InRow || info.InRowCell)
            //{
            //    string colCaption = info.Column == null ? "N/A" : info.Column.GetCaption();
            //    MessageBox.Show(string.Format("DoubleClick on row: {0}, column: {1}.", info.RowHandle, colCaption));
            //}            

            var rowHandle = gvUserDept.FocusedRowHandle;

            // Get the value for the given column - convert to the type you're expecting
            //var ud_Code = gvUserDept.GetRowCellValue(rowHandle, "ud_Code");
            //var ud_Name = gvUserDept.GetRowCellValue(rowHandle, "ud_Name");
            //var ud_inactive = gvUserDept.GetRowCellValue(rowHandle, "ud_inactive");

            //txtCode.Text = ud_Code.ToString();
            //txtName.Text = ud_Name.ToString();
            //chkInactive.EditValue = Convert.ToBoolean(ud_inactive);

        }

        private void gcUserDept_Click(object sender, EventArgs e)
        {

        }
    }
}